#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/**
 * @file mainwindow.h
 * @brief Header of the MainWindow class
 */

#include "phonebook_global.h"

#include <QMainWindow>
#include <QSqlTableModel>
#include <QSortFilterProxyModel>

namespace Ui {
    class MainWindow;
}

/**
 * @class GenericPairingPhonebook::MainWindow
 * @brief This class represents the main window of the phonebook front-end
 */
class GenericPairingPhonebook::MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QSqlTableModel *model, QWidget *parent = 0);
    ~MainWindow();

private:
    QSqlTableModel* dbModel;
    QSortFilterProxyModel* sortedModel;
    Ui::MainWindow *ui;

    // called on selection of contacts
    /**
     * @brief Checks for hasPairing in database and enables/disables create/remove pairing buttons
     *
     * Enables either the CreatePairing or the RemovePairing button and disables the other one
     */
    void setPairingButtons(); // Checks for hasPairing
    /**
     * @brief Generates contact info formlayout rows for selected contact
     *
     * Creates/edits the rows in the contact info formLayout where each row corresponds to a column
     * of the database for the selected contact. Hides empty and unused (previously added before one or
     * more database columns got deleted) rows.
     */
    void setFormLayoutRows(); // Generates the contactInfo formlayout

    // button functions
    /**
     * @brief Creates new contact "New contact..."
     */
    void createNewContact();
    /**
     * @brief Deletes currently selected contact
     */
    void deleteContact();
    /**
     * @brief Opens pairing dialog for currently selected contact
     */
    void createPairing();
    /**
     * @brief Removes pairing data of currently selected contact
     */
    void removePairing();
    /**
     * @brief Shows empty rows in contact info formlayout
     *
     * "Active" rows are rows in the contact info formlayout that correspond to non-deleted columns of the database model
     */
    void showAllActiveRows();
    /**
     * @brief Saves changes in contact info formlayout rows to database
     */
    void saveChanges();

    // helper functions
    /**
     * @brief Selects contact
     * @param row of contact (in contact listview)
     */
    void selectContact(int row);
    /**
     * @brief Hides empty rows in contact info formlayout
     */
    void hideEmptyRows();
    /**
     * @brief Returns contact listview row for given contact name
     * @param contactName
     * @return corresponding row of contact in contact listview
     */
    int getRowFromContactName(QString contactName);
};

#endif // MAINWINDOW_H
