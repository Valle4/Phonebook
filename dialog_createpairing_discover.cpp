#include "dialog_createpairing_discover.h"
#include "ui_dialog_createpairing_discover.h"

#include "../GenericPairingLibrary/genericpairing.h"

#include <QLabel>
#include <QLineEdit>
#include <QRadioButton>
#include <QListWidget>

#define METHOD_MDNSSD 0
#define METHOD_IP 1

using GenericPairingPhonebook::Dialog_CreatePairing_Discover;

Dialog_CreatePairing_Discover::Dialog_CreatePairing_Discover(QZeroConf* zeroConf, QWidget* parent) :
    zeroConf(zeroConf),
    QDialog(parent),
    ui(new Ui::Dialog_CreatePairing_Discover)
{
    ui->setupUi(this);
#ifdef Q_OS_ANDROID
    ui->verticalLayout->setContentsMargins(10,10,10,80);
    this->setWindowState(Qt::WindowFullScreen);
#endif // Q_OS_ANDROID
    changeFields(ui->comboBox->currentIndex());
    connect(ui->comboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &Dialog_CreatePairing_Discover::changeFields);
}

Dialog_CreatePairing_Discover::~Dialog_CreatePairing_Discover()
{
    delete ui;
}



bool Dialog_CreatePairing_Discover::isClient(){
    return willSendRequest;
}

bool Dialog_CreatePairing_Discover::mustPublishMdnssdService(){
    return (ui->comboBox->currentIndex() == METHOD_MDNSSD) && !willSendRequest;
}

QString Dialog_CreatePairing_Discover::getIp(){
    return ip;
}

QString Dialog_CreatePairing_Discover::getPort(){
    return port;
}




void Dialog_CreatePairing_Discover::changeFields(int chosenMethod){

    // First remove old widgets
    QLayoutItem* child;
    while (ui->verticalLayout->count() > 4){ // Label+Combobox + Vspacer+Dialogbuttons, remove other widgets
        child = ui->verticalLayout->takeAt(2); // After Combobox
        delete child->widget();
        delete child;
    }

    // Also deactivate service browsing if mDNS-SD is not the selected method
    if (chosenMethod != METHOD_MDNSSD){
        zeroConf->stopBrowser();
        zeroConf->disconnect();
        serviceInstances.clear();
    }

    // Now create new widgets
    int insertIndex = 2; // After Combobox
    switch (chosenMethod) {

        case METHOD_MDNSSD: {
            QRadioButton* mdnssd_radioButton_listen = new QRadioButton("Publish service and wait for request");
            QRadioButton* mdnssd_radioButton_send = new QRadioButton("Select service instance and send request");
            QListWidget* mdnssd_listWidget_selectServiceInstance = new QListWidget;
            mdnssd_listWidget_selectServiceInstance->setSelectionMode(QAbstractItemView::SingleSelection);
            ui->verticalLayout->insertWidget(insertIndex++, mdnssd_radioButton_listen);
            ui->verticalLayout->insertWidget(insertIndex++, mdnssd_radioButton_send);
            ui->verticalLayout->insertWidget(insertIndex++, mdnssd_listWidget_selectServiceInstance);

            // Start the mDNS-SD service browser
            connect(zeroConf, &QZeroConf::serviceAdded, [this, mdnssd_listWidget_selectServiceInstance](QZeroConfService* addedService){
                serviceInstances.append(addedService);
                mdnssd_listWidget_selectServiceInstance->addItem(addedService->name);
            });
            connect(zeroConf, &QZeroConf::serviceRemoved, [this, mdnssd_listWidget_selectServiceInstance](QZeroConfService* removedService){
                serviceInstances.removeOne(removedService);
                delete mdnssd_listWidget_selectServiceInstance->findItems(removedService->name,Qt::MatchFixedString).first();
            });
            zeroConf->startBrowser(QString::fromStdString(GenericPairing::MdnssdServiceType));
            connect(this, &Dialog_CreatePairing_Discover::finished, zeroConf, &QZeroConf::stopBrowser);

            // Edit the private ip and port variables on selection
            connect(mdnssd_listWidget_selectServiceInstance, &QListWidget::currentRowChanged, [this](int row){
                ip = serviceInstances.at(row)->ip.toString();
                port = QString::number(serviceInstances.at(row)->port);
            });

            // Enable list widget only when radiobutton_send is selected
            connect(mdnssd_radioButton_send, &QRadioButton::toggled, mdnssd_listWidget_selectServiceInstance, &QListWidget::setEnabled);

            // Set willSendRequest and depending on which role was selected
            connect(mdnssd_radioButton_send, &QRadioButton::toggled, [this](bool clientRoleSelected){willSendRequest = clientRoleSelected;});

            // Select radiobutton_listen per default
            mdnssd_radioButton_listen->setChecked(true);
            emit mdnssd_radioButton_send->toggled(false);


            break;
        }

        case METHOD_IP: {
            QRadioButton* ip_radioButton_listen = new QRadioButton("Listen for pairing request");
            QRadioButton* ip_radioButton_send = new QRadioButton("Send pairing request");
            QLabel* ip_label_ip = new QLabel("IP");
            QLabel* ip_label_port = new QLabel("Port");
            QLineEdit* ip_lineEdit_ip = new QLineEdit;
            QLineEdit* ip_lineEdit_port = new QLineEdit(QString::number(GenericPairing::DefaultPort));
            ui->verticalLayout->insertWidget(insertIndex++, ip_radioButton_listen);
            ui->verticalLayout->insertWidget(insertIndex++, ip_radioButton_send);
            ui->verticalLayout->insertWidget(insertIndex++, ip_label_ip);
            ui->verticalLayout->insertWidget(insertIndex++, ip_lineEdit_ip);
            ui->verticalLayout->insertWidget(insertIndex++, ip_label_port);
            ui->verticalLayout->insertWidget(insertIndex++, ip_lineEdit_port);

            // Enable ip:port widgets only when radiobutton_send is selected
            connect(ip_radioButton_send, &QRadioButton::toggled, ip_label_ip, &QLabel::setEnabled);
            connect(ip_radioButton_send, &QRadioButton::toggled, ip_lineEdit_ip, &QLabel::setEnabled);
            connect(ip_radioButton_send, &QRadioButton::toggled, ip_label_port, &QLabel::setEnabled);
            connect(ip_radioButton_send, &QRadioButton::toggled, ip_lineEdit_port, &QLabel::setEnabled);

            // Set willSendRequest depending on which role was selected
            connect(ip_radioButton_send, &QRadioButton::toggled, [this](bool clientRoleSelected){willSendRequest = clientRoleSelected;});

            // Select radiobutton_listen per default
            ip_radioButton_listen->setChecked(true);
            emit ip_radioButton_send->toggled(false);

            // Let the line edits edit the private ip and port variables automatically
            connect(ip_lineEdit_ip, &QLineEdit::textEdited, [this](const QString& text){ip = text;});
            connect(ip_lineEdit_port, &QLineEdit::textEdited, [this](const QString& text){port = text;});

            break;
        }
    }
}
