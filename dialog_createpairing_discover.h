#ifndef DIALOG_CREATEPAIRING_DISCOVER_H
#define DIALOG_CREATEPAIRING_DISCOVER_H

/**
 * @file dialog_createpairing_discover.h
 * @brief Header of the Dialog_CreatePairing_Discover class
 */

#include "phonebook_global.h"

#include "../GenericPairingLibrary/genericpairing.h"

#include <QDialog>
#include <qzeroconf.h>

namespace Ui {
    class Dialog_CreatePairing_Discover;
}

/**
 * @class GenericPairingPhonebook::Dialog_CreatePairing_Discover
 * @brief This class represents the discovery dialog of the pairing process
 *
 * The user can choose to either wait for a pairing request or
 * discover the 2nd user and send the pairing request to him.
 *
 * This dialog currently offers 2 ways of discovery:
 *   - mDNS-SD method (find service that is published by 2nd user)
 *   - IP method (enter IP and port of 2nd user manually)
 */
class GenericPairingPhonebook::Dialog_CreatePairing_Discover : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_CreatePairing_Discover(QZeroConf* zeroConf, QWidget* parent = 0);
    ~Dialog_CreatePairing_Discover();


    /**
     * @brief Checks if user selected to initiate pairing or not
     *
     * If the user selected to send the pairing request to a discovered IP and port,
     * he is considered the client;
     * if he chose to wait for a request, he is considered the server.
     *
     * @return true if client, false if server
     */
    bool isClient();
    /**
     * @brief Checks if user selected mDNS-SD and waiting for request
     * @return true if mDNS-SD and server, false otherwise
     */
    bool mustPublishMdnssdService();
    /**
     * @brief Returns discovered IP address
     * @return discovered IP address
     */
    QString getIp();
    /**
     * @brief Returns discovered port
     * @return discovered port
     */
    QString getPort();

private:
    Ui::Dialog_CreatePairing_Discover *ui;

    // For mDNS-SD method
    QZeroConf* zeroConf;
    QList<QZeroConfService*> serviceInstances;

    bool willSendRequest;
    QString ip;
    QString port = QString::number(GenericPairing::DefaultPort);


    /**
     * @brief Changes dialog fields depending on chosen method
     * @param chosenMethod
     */
    void changeFields(int chosenMethod);
};

#endif // DIALOG_CREATEPAIRING_DISCOVER_H
