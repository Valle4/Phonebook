#-------------------------------------------------
#
# Project created by QtCreator 2016-04-12T20:07:03
#
#-------------------------------------------------

include(../QtZeroConf/qtzeroconf.pri)

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = phonebook
CONFIG   += c++11

linux:!android {
    message("* Using settings for Unix/Linux.")
    LIBS += -L"../build-GenericPairingLibrary-Desktop-Debug/"
}

android {
    message("* Using settings for Android.")
    LIBS += -L"../build-GenericPairingLibrary-Android_f_r_armeabi_v7a_GCC_4_9_Qt_5_7_0-Debug"
}

LIBS += -lgenericpairing

TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog_createpairing_discover.cpp \
    dialog_createpairing_sas.cpp

HEADERS  += mainwindow.h \
    dialog_createpairing_discover.h \
    dialog_createpairing_sas.h \
    phonebook_global.h

FORMS    += mainwindow.ui \
    dialog_createpairing_discover.ui \
    dialog_createpairing_sas.ui

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_EXTRA_LIBS = \
        ../build-GenericPairingLibrary-Android_f_r_armeabi_v7a_GCC_4_9_Qt_5_7_0-Debug/libgenericpairing.so \
}
