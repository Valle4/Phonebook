#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "dialog_createpairing_discover.h"
#include "dialog_createpairing_sas.h"

#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QSqlRecord>

#define DBCOL_CONTACTNAME 0
#define DBCOL_HASPAIRING 1
#define DBCOL_FORMLAYOUT_FIRSTROW 2



using GenericPairingPhonebook::MainWindow;



MainWindow::MainWindow(QSqlTableModel *model, QWidget *parent) :
    QMainWindow(parent),
    dbModel(model),
    sortedModel(new QSortFilterProxyModel(this)),
    ui(new Ui::MainWindow)
{
    dbModel->setEditStrategy(QSqlTableModel::OnFieldChange); // no OnManualSubmit because contact name should be directly changeable from listView
    // The only reason to use a proxy model here is that changing the contact name will trigger üsorting of the contact list this way
    sortedModel->setSourceModel(dbModel);
    sortedModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    sortedModel->sort(1);



    ui->setupUi(this);

    ui->listView_contactList->setModel(sortedModel);
    ui->listView_contactList->setModelColumn(DBCOL_CONTACTNAME);

    // adjust default splitter sizes to 50:50
    QList<int> sizesList = {1,1};
    ui->splitter->setSizes(sizesList);

    // hide showEmptyFields button initially
    ui->pushButton_showEmptyFields->hide();



    // the formlayout rows will be created/changed when the current selection changes
    QItemSelectionModel *selectionModel = ui->listView_contactList->selectionModel();
    connect(selectionModel, &QItemSelectionModel::currentChanged, this, &MainWindow::setPairingButtons);
    connect(selectionModel, &QItemSelectionModel::currentChanged, this, &MainWindow::setFormLayoutRows);
    if (sortedModel->rowCount() > 0){ // select first contact if existent
        selectContact(0);
    } else { // create new contact per default
        createNewContact();
    }

    // connect buttons to their functions
    #ifdef Q_OS_ANDROID
    connect(ui->pushButton_newContact, &QAbstractButton::pressed, this, &MainWindow::createNewContact);
    connect(ui->pushButton_deleteContact, &QAbstractButton::pressed, this, &MainWindow::deleteContact);
    connect(ui->pushButton_createPairing, &QAbstractButton::pressed, this, &MainWindow::createPairing);
    connect(ui->pushButton_removePairing, &QAbstractButton::pressed, this, &MainWindow::removePairing);
    connect(ui->pushButton_showEmptyFields, &QAbstractButton::pressed, this, &MainWindow::showAllActiveRows);
    connect(ui->pushButton_saveChanges, &QAbstractButton::pressed, this, &MainWindow::saveChanges);
    #else
    connect(ui->pushButton_newContact, &QAbstractButton::clicked, this, &MainWindow::createNewContact);
    connect(ui->pushButton_deleteContact, &QAbstractButton::clicked, this, &MainWindow::deleteContact);
    connect(ui->pushButton_createPairing, &QAbstractButton::clicked, this, &MainWindow::createPairing);
    connect(ui->pushButton_removePairing, &QAbstractButton::clicked, this, &MainWindow::removePairing);
    connect(ui->pushButton_showEmptyFields, &QAbstractButton::clicked, this, &MainWindow::showAllActiveRows);
    connect(ui->pushButton_saveChanges, &QAbstractButton::clicked, this, &MainWindow::saveChanges);
    #endif
}

MainWindow::~MainWindow()
{
    delete ui;
    delete sortedModel;
}




// Enables either the CreatePairing or the RemovePairing button and disables the other one
void MainWindow::setPairingButtons(){
    int listViewCurrentRow = ui->listView_contactList->currentIndex().row();
    bool hasPairing = sortedModel->index(listViewCurrentRow,DBCOL_HASPAIRING).data().toBool();

    ui->pushButton_createPairing->setEnabled(!hasPairing);
    ui->pushButton_removePairing->setEnabled(hasPairing);
}

// Creates/edits the rows in formLayout, hides empty and unused rows
void MainWindow::setFormLayoutRows(){
    int listViewCurrentRow = ui->listView_contactList->currentIndex().row();
    int dbColumnCount = sortedModel->columnCount();
    int formLayoutRowCount = ui->formLayout_contactInfo->rowCount();

    QString dbColName;
    QString lineEditContent;
    QLineEdit* lineEdit;
    QLabel* label;

    // already existing fields might have been hidden previously if they were empty but might now get content
    showAllActiveRows();

    // create/edit all formLayout fields that we need
    int formLayoutRowIndex;
    for (int dbCol = DBCOL_FORMLAYOUT_FIRSTROW; dbCol < dbColumnCount; ++dbCol){ // column 0 is the contact name and is not shown in formLayout
        formLayoutRowIndex = dbCol-DBCOL_FORMLAYOUT_FIRSTROW;

        dbColName = sortedModel->headerData(dbCol, Qt::Horizontal, Qt::DisplayRole).toString();
        lineEditContent = sortedModel->index(listViewCurrentRow,dbCol).data().toString();

        if (formLayoutRowIndex < formLayoutRowCount){ // row exists, just overwrite the content of its widgets
            label = (QLabel*) ui->formLayout_contactInfo->itemAt(formLayoutRowIndex,QFormLayout::LabelRole)->widget();
            label->setText(dbColName);
            lineEdit = (QLineEdit*) ui->formLayout_contactInfo->itemAt(formLayoutRowIndex,QFormLayout::FieldRole)->widget();
            lineEdit->setText(lineEditContent);
        }else{ // we need to add an extra row to formLayout
            lineEdit = new QLineEdit(lineEditContent);
            ui->formLayout_contactInfo->addRow(dbColName, lineEdit);
        }
    }

    // there might been additional rows from before a db-column was deleted, hide those
    while (++formLayoutRowIndex < formLayoutRowCount){
        lineEdit = (QLineEdit*) ui->formLayout_contactInfo->itemAt(formLayoutRowIndex,QFormLayout::FieldRole)->widget();
        lineEdit->hide();
        ui->formLayout_contactInfo->labelForField(lineEdit)->hide();
    }

    hideEmptyRows();
}



void MainWindow::createNewContact(){
    QSqlRecord rec = dbModel->record();
    rec.setValue(DBCOL_CONTACTNAME,"New contact...");
    rec.setGenerated(DBCOL_CONTACTNAME,true);
    dbModel->insertRecord(-1,rec);
    sortedModel->sort(DBCOL_CONTACTNAME);

    // select the newly created record
    int dbRowCount = sortedModel->rowCount();
    for (int row = 0; row < dbRowCount; ++row){
        if (sortedModel->index(row,DBCOL_CONTACTNAME).data().toString() == "New contact..."){
            selectContact(row);
            break;
        }
    }

    showAllActiveRows(); // rows do exist because we changed the selection and thus called setFormLayoutRows()
}

void MainWindow::deleteContact(){
    int row = ui->listView_contactList->currentIndex().row();
    sortedModel->removeRow(row);
    dbModel->select(); // needed to refresh after deleting

    if (sortedModel->rowCount() > row){ // select next contact if existent
        selectContact(row);
    } else if (sortedModel->rowCount() > 0){ // select previous contact otherwise (if this was not the last contact)
        selectContact(row-1);
    } else { // create new contact per default if we just deleted the last one
        createNewContact();
    }
}

void MainWindow::createPairing(){
    int row = ui->listView_contactList->currentIndex().row();
    QByteArray contactName = sortedModel->index(row,DBCOL_CONTACTNAME).data().toByteArray();
    QZeroConf zeroConf;

    Dialog_CreatePairing_Discover discoveryDialog(&zeroConf);
    if (discoveryDialog.exec() != QDialog::Accepted)
        return; // Cancelled

    Dialog_CreatePairing_Sas sasDialog(&zeroConf,
                                       discoveryDialog.isClient(),
                                       discoveryDialog.mustPublishMdnssdService(),
                                       contactName,
                                       QByteArray::fromStdString(discoveryDialog.getIp().toStdString()),
                                       discoveryDialog.getPort().toInt());

    if (sasDialog.exec() == QDialog::Accepted){ // Pairing successful
        sortedModel->setData(sortedModel->index(row,DBCOL_HASPAIRING),true);
        setPairingButtons();
    }
}

void MainWindow::removePairing(){
    int row = ui->listView_contactList->currentIndex().row();
    QByteArray contactName = sortedModel->index(row,DBCOL_CONTACTNAME).data().toByteArray();

#ifdef Q_OS_ANDROID
    QTcpSocket* ipcSocket = new QTcpSocket;
#else
    QLocalSocket* ipcSocket = new QLocalSocket;
#endif // Q_OS_ANDROID

    auto sendMessage = [ipcSocket, contactName]{
        QDataStream localStream(ipcSocket);
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Request_RemovePairing
                    << contactName
                    << "\n";
    };

    auto readAnswer = [this, ipcSocket, contactName]{
        if (!ipcSocket->canReadLine())
            return;

        QDataStream localStream(ipcSocket);
        GenericPairing::message_t message_raw;
        localStream >> message_raw;
        ipcSocket->deleteLater();
        GenericPairing::IpcMessage message = (GenericPairing::IpcMessage) message_raw;
        if (message == GenericPairing::IpcMessage::Success){
            sortedModel->setData(sortedModel->index(getRowFromContactName(contactName),DBCOL_HASPAIRING),false);
            setPairingButtons();
        }
    };

#ifdef Q_OS_ANDROID
    connect(ipcSocket, &QTcpSocket::connected, sendMessage);
    connect(ipcSocket, &QTcpSocket::readyRead, readAnswer);
    ipcSocket->connectToHost("localhost",GenericPairing::DefaultLoopbackIpcPort);
#else
    connect(ipcSocket, &QLocalSocket::connected, sendMessage);
    connect(ipcSocket, &QLocalSocket::readyRead, readAnswer);
    ipcSocket->connectToServer(QString::fromStdString(GenericPairing::IpcServerName));
#endif // Q_OS_ANDROID
}

// "Active" rows are rows in formlayout that correspond to non-deleted columns of the database model
void MainWindow::showAllActiveRows(){
    int formLayoutRowCount = ui->formLayout_contactInfo->rowCount();
    int dbModelColumnCountToShow = sortedModel->columnCount()-DBCOL_FORMLAYOUT_FIRSTROW;

    // leftover rows in formlayout from db-column deletions will NOT be shown
    int rowCount = (formLayoutRowCount > dbModelColumnCountToShow) ? dbModelColumnCountToShow : formLayoutRowCount;

    QLineEdit *field;
    for (int row = 0; row < rowCount; ++row){
        field = (QLineEdit*) ui->formLayout_contactInfo->itemAt(row,QFormLayout::FieldRole)->widget();
        field->show();
        ui->formLayout_contactInfo->labelForField(field)->show();
    }
    ui->pushButton_showEmptyFields->hide();
}

void MainWindow::saveChanges(){
    QPersistentModelIndex modelIndex(sortedModel->mapToSource(ui->listView_contactList->currentIndex())); // for consistent row()
    QSqlRecord rec = dbModel->record(modelIndex.row());
    QLineEdit* lineEdit;

    int dbColumnCount = dbModel->columnCount();
    int formLayoutRowIndex;
    for (int dbCol = DBCOL_FORMLAYOUT_FIRSTROW; dbCol < dbColumnCount; ++dbCol){
        formLayoutRowIndex = dbCol-DBCOL_FORMLAYOUT_FIRSTROW;
        lineEdit = (QLineEdit*) ui->formLayout_contactInfo->itemAt(formLayoutRowIndex,QFormLayout::FieldRole)->widget();
        rec.setValue(dbCol,lineEdit->text());
        rec.setGenerated(dbCol,true);
    }

    dbModel->setRecord(modelIndex.row(),rec);
    selectContact(sortedModel->mapFromSource(modelIndex).row());
}



void MainWindow::selectContact(int row){
    QItemSelectionModel *selectionModel = ui->listView_contactList->selectionModel();
    QModelIndex contactIndex = sortedModel->index(row,DBCOL_CONTACTNAME);
    selectionModel->select(contactIndex,QItemSelectionModel::Select);
    ui->listView_contactList->setCurrentIndex(contactIndex);

    if (sortedModel->index(row,DBCOL_HASPAIRING).data().toBool()){
        ui->pushButton_createPairing->setEnabled(false);
        ui->pushButton_removePairing->setEnabled(true);
    } else {
        ui->pushButton_createPairing->setEnabled(true);
        ui->pushButton_removePairing->setEnabled(false);
    }
}

void MainWindow::hideEmptyRows(){
    int formLayoutRowCount = ui->formLayout_contactInfo->rowCount();
    int dbModelColumnCountToShow = sortedModel->columnCount()-DBCOL_FORMLAYOUT_FIRSTROW;

    // formlayout might have more rows because of previous db-column deletions, those are already hidden
    int rowCount = (formLayoutRowCount > dbModelColumnCountToShow) ? dbModelColumnCountToShow : formLayoutRowCount;
    int hiddenRows = 0;

    QLineEdit *field;
    for (int row = 0; row < rowCount; ++row){
        field = (QLineEdit*) ui->formLayout_contactInfo->itemAt(row,QFormLayout::FieldRole)->widget();
        if (field->text().isEmpty()){
            field->hide();
            ui->formLayout_contactInfo->labelForField(field)->hide();
            ++hiddenRows;
        }
    }
    if (hiddenRows>0)
        ui->pushButton_showEmptyFields->show();
}

int MainWindow::getRowFromContactName(QString contactName){
    QModelIndexList modelIndexList = sortedModel->match(sortedModel->index(0,DBCOL_CONTACTNAME),Qt::DisplayRole,contactName);
    if (modelIndexList.empty())
        return -1;
    return modelIndexList.at(0).row();
}
